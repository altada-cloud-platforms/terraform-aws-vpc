################################################################################
# Managed Services Private routes
# There are as many routing tables as the number of NAT gateways
################################################################################

resource "aws_route_table" "managed_services" {
  count = var.create_vpc && var.create_managed_services_subnet_route_table && length(var.managed_services_subnets) > 0 ? local.nat_gateway_count : 0

  vpc_id = local.vpc_id

  tags = merge(
    {
      "Name" = var.single_nat_gateway ? "${var.name}-${var.managed_services_suffix}" : format(
        "%s-${var.managed_services_suffix}-%s",
        var.name,
        element(var.azs, count.index),
      )
    },
    var.tags,
    var.managed_services_route_table_tags,
  )
}

################################################################################
# EKS Private routes
# There are as many routing tables as the number of NAT gateways
################################################################################

resource "aws_route_table" "eks" {
  count = var.create_vpc && var.create_eks_subnet_route_table && length(var.eks_subnets) > 0 ? local.nat_gateway_count : 0

  vpc_id = local.vpc_id

  tags = merge(
    {
      "Name" = var.single_nat_gateway ? "${var.name}-${var.eks_suffix}" : format(
        "%s-${var.eks_suffix}-%s",
        var.name,
        element(var.azs, count.index),
      )
    },
    var.tags,
    var.eks_route_table_tags,
  )
}

################################################################################
# Managed Services Private subnet
################################################################################

resource "aws_subnet" "managed_services" {
  count = var.create_vpc && length(var.managed_services_subnets) > 0 ? length(var.managed_services_subnets) : 0

  vpc_id                          = local.vpc_id
  cidr_block                      = var.managed_services_subnets[count.index]
  availability_zone               = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) > 0 ? element(var.azs, count.index) : null
  availability_zone_id            = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) == 0 ? element(var.azs, count.index) : null
  assign_ipv6_address_on_creation = var.managed_services_subnet_assign_ipv6_address_on_creation == null ? var.assign_ipv6_address_on_creation : var.managed_services_subnet_assign_ipv6_address_on_creation

  ipv6_cidr_block = var.enable_ipv6 && length(var.managed_services_subnet_ipv6_prefixes) > 0 ? cidrsubnet(aws_vpc.this[0].ipv6_cidr_block, 8, var.managed_services_subnet_ipv6_prefixes[count.index]) : null

  tags = merge(
    {
      "Name" = format(
        "%s-${var.managed_services_subnet_suffix}-%s",
        var.name,
        element(var.azs, count.index),
      )
    },
    var.tags,
    var.managed_services_subnet_tags,
  )
}

################################################################################
# EKS Private subnet
################################################################################

resource "aws_subnet" "eks" {
  count = var.create_vpc && length(var.eks_subnets) > 0 ? length(var.eks_subnets) : 0

  vpc_id                          = local.vpc_id
  cidr_block                      = var.eks_subnets[count.index]
  availability_zone               = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) > 0 ? element(var.azs, count.index) : null
  availability_zone_id            = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) == 0 ? element(var.azs, count.index) : null
  assign_ipv6_address_on_creation = var.eks_subnet_assign_ipv6_address_on_creation == null ? var.assign_ipv6_address_on_creation : var.eks_subnet_assign_ipv6_address_on_creation

  ipv6_cidr_block = var.enable_ipv6 && length(var.eks_subnet_ipv6_prefixes) > 0 ? cidrsubnet(aws_vpc.this[0].ipv6_cidr_block, 8, var.eks_subnet_ipv6_prefixes[count.index]) : null

  tags = merge(
    {
      "Name" = format(
        "%s-${var.eks_subnet_suffix}-%s",
        var.name,
        element(var.azs, count.index),
      )
    },
    var.tags,
    var.eks_subnet_tags,
  )
}


################################################################################
# Managed Services Private Network ACLs
################################################################################

resource "aws_network_acl" "managed_services" {
  count = var.create_vpc && var.managed_services_dedicated_network_acl && length(var.managed_services_subnets) > 0 ? 1 : 0

  vpc_id     = element(concat(aws_vpc.this.*.id, [""]), 0)
  subnet_ids = aws_subnet.managed_services.*.id

  tags = merge(
    {
      "Name" = format("%s-${var.managed_services_subnet_suffix}", var.name)
    },
    var.tags,
    var.managed_services_acl_tags,
  )
}

resource "aws_network_acl_rule" "managed_services_inbound" {
  count = var.create_vpc && var.managed_services_dedicated_network_acl && length(var.managed_services_subnets) > 0 ? length(var.managed_services_inbound_acl_rules) : 0

  network_acl_id = aws_network_acl.managed_services[0].id

  egress          = false
  rule_number     = var.managed_services_inbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.managed_services_inbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.managed_services_inbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.managed_services_inbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.managed_services_inbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.managed_services_inbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.managed_services_inbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.managed_services_inbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.managed_services_inbound_acl_rules[count.index], "ipv6_cidr_block", null)
}

resource "aws_network_acl_rule" "managed_services_outbound" {
  count = var.create_vpc && var.managed_services_dedicated_network_acl && length(var.managed_services_subnets) > 0 ? length(var.managed_services_outbound_acl_rules) : 0

  network_acl_id = aws_network_acl.managed_services[0].id

  egress          = true
  rule_number     = var.managed_services_outbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.managed_services_outbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.managed_services_outbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.managed_services_outbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.managed_services_outbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.managed_services_outbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.managed_services_outbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.managed_services_outbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.managed_services_outbound_acl_rules[count.index], "ipv6_cidr_block", null)
}

################################################################################
# EKS Private Network ACLs
################################################################################

resource "aws_network_acl" "eks" {
  count = var.create_vpc && var.eks_dedicated_network_acl && length(var.eks_subnets) > 0 ? 1 : 0

  vpc_id     = element(concat(aws_vpc.this.*.id, [""]), 0)
  subnet_ids = aws_subnet.eks.*.id

  tags = merge(
    {
      "Name" = format("%s-${var.eks_subnet_suffix}", var.name)
    },
    var.tags,
    var.eks_acl_tags,
  )
}

resource "aws_network_acl_rule" "eks_inbound" {
  count = var.create_vpc && var.eks_dedicated_network_acl && length(var.eks_subnets) > 0 ? length(var.eks_inbound_acl_rules) : 0

  network_acl_id = aws_network_acl.eks[0].id

  egress          = false
  rule_number     = var.eks_inbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.eks_inbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.eks_inbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.eks_inbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.eks_inbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.eks_inbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.eks_inbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.eks_inbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.eks_inbound_acl_rules[count.index], "ipv6_cidr_block", null)
}

resource "aws_network_acl_rule" "eks_outbound" {
  count = var.create_vpc && var.eks_dedicated_network_acl && length(var.eks_subnets) > 0 ? length(var.eks_outbound_acl_rules) : 0

  network_acl_id = aws_network_acl.eks[0].id

  egress          = true
  rule_number     = var.eks_outbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.eks_outbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.eks_outbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.eks_outbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.eks_outbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.eks_outbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.eks_outbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.eks_outbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.eks_outbound_acl_rules[count.index], "ipv6_cidr_block", null)
}


################################################################################
# Route table association
################################################################################

resource "aws_route_table_association" "managed_services" {
  count = var.create_vpc && length(var.managed_services_subnets) > 0 ? length(var.managed_services_subnets) : 0

  subnet_id = element(aws_subnet.managed_services.*.id, count.index)
  route_table_id = element(
    coalescelist(aws_route_table.managed_services.*.id, aws_route_table.private.*.id),
    var.single_nat_gateway || var.create_managed_services_subnet_route_table ? 0 : count.index,
  )
}

resource "aws_route_table_association" "eks" {
  count = var.create_vpc && length(var.eks_subnets) > 0 ? length(var.eks_subnets) : 0

  subnet_id = element(aws_subnet.eks.*.id, count.index)
  route_table_id = element(
    coalescelist(aws_route_table.eks.*.id, aws_route_table.private.*.id),
    var.single_nat_gateway || var.create_eks_subnet_route_table ? 0 : count.index,
  )
}

################################################################################
# VPN Gateway
################################################################################
resource "aws_vpn_gateway_route_propagation" "managed_services" {
  count = var.create_vpc && var.propagate_managed_services_route_tables_vgw && (var.enable_vpn_gateway || var.vpn_gateway_id != "") ? length(var.managed_services_subnets) : 0

  route_table_id = element(aws_route_table.managed_services.*.id, count.index)
  vpn_gateway_id = element(
    concat(
      aws_vpn_gateway.this.*.id,
      aws_vpn_gateway_attachment.this.*.vpn_gateway_id,
    ),
    count.index,
  )
}

resource "aws_vpn_gateway_route_propagation" "eks" {
  count = var.create_vpc && var.propagate_eks_route_tables_vgw && (var.enable_vpn_gateway || var.vpn_gateway_id != "") ? length(var.eks_subnets) : 0

  route_table_id = element(aws_route_table.eks.*.id, count.index)
  vpn_gateway_id = element(
    concat(
      aws_vpn_gateway.this.*.id,
      aws_vpn_gateway_attachment.this.*.vpn_gateway_id,
    ),
    count.index,
  )
}
